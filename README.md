# Wishlist Template

This project was originally developed to create templates for wishlists. The idea is that you would create an entity
called "Wishlist template" and add taxonomy terms (that are represented in some catalog) and default products. When
you view the wishlist template entity, it currently looks for an existing wishlist that is connected with your template
and if it doesn't find one, it presents a form. That form has a button, that when clicked, creates a new wishlist with
the default products.

It is functional, but there are some very rough edges.
