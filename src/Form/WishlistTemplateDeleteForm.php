<?php

namespace Drupal\wishlist_template\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Wishlist template entities.
 *
 * @ingroup wishlist_template
 */
class WishlistTemplateDeleteForm extends ContentEntityDeleteForm {


}
