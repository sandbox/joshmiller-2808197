<?php

namespace Drupal\wishlist_template\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_wishlist\WishlistManagerInterface;
use Drupal\commerce_wishlist\WishlistProviderInterface;
use Drupal\commerce_order\Resolver\OrderTypeResolverInterface;
use Drupal\commerce_store\StoreContextInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\wishlist_template\Entity\WishlistTemplate;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CreateWishlistBasedOnTemplateForm extends FormBase {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * The wishlist manager.
   *
   * @var \Drupal\commerce_wishlist\WishlistManagerInterface
   */
  protected $wishlistManager;

  /**
   * The wishlist provider.
   *
   * @var \Drupal\commerce_wishlist\WishlistProviderInterface
   */
  protected $wishlistProvider;

  /**
   * The order type resolver.
   *
   * @var \Drupal\commerce_order\Resolver\OrderTypeResolverInterface
   */
  protected $orderTypeResolver;

  /**
   * The store context.
   *
   * @var \Drupal\commerce_store\StoreContextInterface
   */
  protected $storeContext;

  /**
   * Constructs a new AddToWishlistForm object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\commerce_wishlist\WishlistManagerInterface $wishlist_manager
   *   The cart manager.
   * @param \Drupal\commerce_wishlist\WishlistProviderInterface $wishlist_provider
   *   The cart provider.
   * @param \Drupal\commerce_order\Resolver\OrderTypeResolverInterface $order_type_resolver
   *   The order type resolver.
   * @param \Drupal\commerce_store\StoreContextInterface $store_context
   *   The store context.
   */
  public function __construct(EntityManagerInterface $entity_manager, WishlistManagerInterface $wishlist_manager, WishlistProviderInterface $wishlist_provider, OrderTypeResolverInterface $order_type_resolver, StoreContextInterface $store_context) {

    $this->entityManager = $entity_manager;
    $this->wishlistManager = $wishlist_manager;
    $this->wishlistProvider = $wishlist_provider;
    $this->orderTypeResolver = $order_type_resolver;
    $this->storeContext = $store_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('commerce_wishlist.wishlist_manager'),
      $container->get('commerce_wishlist.wishlist_provider'),
      $container->get('commerce_order.chain_order_type_resolver'),
      $container->get('commerce_store.store_context')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wishlist_template_create_or_update';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\wishlist_template\Entity\WishlistTemplateInterface $wishlist_template
   *
   * @return array The form structure.
   * The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $wishlist_template = NULL) {

    $form['entity'] = array(
      '#type' => 'hidden',
      '#default_value' => $wishlist_template->id(),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Start Shopping using this template.'),
    );

    return $form;

  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /**
     * @var \Drupal\wishlist_template\Entity\WishlistTemplateInterface $wishlist_template
     * @var \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem[] $default_products
     */
    $wishlist_template = WishlistTemplate::load($form_state->getValue('entity'));
    $default_products = $wishlist_template->get('default_products')->referencedEntities();
    if ($default_products) {
      // Get/Create line item(s).
      $purchased_entities = array();
      $order_items = array();
      foreach ($default_products as $product) {
        /**
         * @var \Drupal\commerce_product\Entity\ProductInterface $product
         * @var \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation
         * @var \Drupal\commerce_order\OrderItemStorageInterface $order_item_storage
         */
        $product_variation = $product->getDefaultVariation();
        $order_item_storage = $this->entityManager->getStorage('commerce_order_item');
        $purchased_entities[] = $product_variation;
        $order_item = $order_item_storage->createFromPurchasableEntity($product_variation);

        // Now that the purchased entity is set, populate the title and price.
        $order_item->setTitle($product_variation->getOrderItemTitle());
        // @todo Remove once the price calculation is in place.
        $order_item->unit_price = $product_variation->price;
        $order_items[] = $order_item;
      }

      // Use first default product for everything (facepalm).
      $order_type = $this->orderTypeResolver->resolve($order_items[0]);
      $store = $this->selectStore($purchased_entities[0]);
      $wishlist = $this->wishlistProvider->getWishlist($order_type, $store);
      if (!$wishlist) {
        $wishlist = $this->wishlistProvider->createWishlist($order_type, $store);
      }

      // Determine if wishlist has a field that can connect w/ template.
      $wishlist_fields = array_keys($wishlist->getFields());
      foreach ($wishlist_fields as $wishlist_field) {
        $order_wishlist_template_reference_field = $wishlist->get($wishlist_field);
        // Only interested in Entity References that target wishlist_templates.
        if ($order_wishlist_template_reference_field->getFieldDefinition()->getType() == "entity_reference" &&
          $order_wishlist_template_reference_field->getItemDefinition()->getSetting("target_type") == "wishlist_template") {
          // The variable $field now has the field we will use to
          // connect the template to the order.
          break;
        }
        $order_wishlist_template_reference_field = FALSE;
      }

      if ($order_wishlist_template_reference_field !== FALSE) {
        // Connect wishlist to this template.
        $wishlist->set($order_wishlist_template_reference_field->getName(),$wishlist_template->id());

        // Save the wishlist.
        $wishlist->save();

        // Add the default products (or increment them if they already exist.
        foreach ($order_items as $order_item) {
          $this->wishlistManager->addOrderItem($wishlist, $order_item, TRUE);
        }
        // @todo Make this success message configurable.
        //drupal_set_message("Your wishlist is now using the " . $wishlist_template->getName() . "!");
      } else {
        drupal_set_message($this->t('The order type ' . $order_type . ' must have an entity_reference that accepts a single wishlist_template.'),"error");
      }
    } else {
      drupal_set_message($this->t('Could not initiate your wishlist template because it has no default products.'),"error");
    }

    // TODO: Implement submitForm() method.
    // drupal_set_message("submission happened.");
  }

  /**
   * Selects the store for the given purchasable entity.
   *
   * If the entity is sold from one store, then that store is selected.
   * If the entity is sold from multiple stores, and the current store is
   * one of them, then that store is selected.
   *
   * @param \Drupal\commerce\PurchasableEntityInterface $entity
   *   The entity being added to cart.
   *
   * @throws \Exception
   *   When the entity can't be purchased from the current store.
   *
   * @return \Drupal\commerce_store\Entity\StoreInterface
   *   The selected store.
   */
  protected function selectStore(PurchasableEntityInterface $entity) {
    $stores = $entity->getStores();
    if (count($stores) === 1) {
      $store = reset($stores);
    }
    else {
      $store = $this->storeContext->getStore();
      if (!in_array($store, $stores)) {
        // Indicates that the site listings are not filtered properly.
        throw new \Exception("The given entity can't be purchased from the current store.");
      }
    }

    return $store;
  }
}