<?php

namespace Drupal\wishlist_template\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Wishlist template type entities.
 */
interface WishlistTemplateTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
