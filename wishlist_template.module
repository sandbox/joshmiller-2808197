<?php

/**
 * @file
 * Contains wishlist_template.module..
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function wishlist_template_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the wishlist_template module.
    case 'help.page.wishlist_template':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Description') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function wishlist_template_theme() {
  $theme = [];
  $theme['wishlist_template'] = array(
    'render element' => 'elements',
    'file' => 'wishlist_template.page.inc',
    'template' => 'wishlist_template',
  );
  $theme['wishlist_template_content_add_list'] = [
    'render element' => 'content',
    'variables' => ['content' => NULL],
    'file' => 'wishlist_template.page.inc',
  ];
  return $theme;
}

/**
* Implements hook_theme_suggestions_HOOK().
*/
function wishlist_template_theme_suggestions_wishlist_template(array $variables) {
  $suggestions = array();
  $entity = $variables['elements']['#wishlist_template'];
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'wishlist_template__' . $sanitized_view_mode;
  $suggestions[] = 'wishlist_template__' . $entity->bundle();
  $suggestions[] = 'wishlist_template__' . $entity->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'wishlist_template__' . $entity->id();
  $suggestions[] = 'wishlist_template__' . $entity->id() . '__' . $sanitized_view_mode;
  return $suggestions;
}

/**
 * Callback function that returns a list of taxonomy view modes.
 */
function _wishlist_template_term_view_mode_values() {
  // Grab the View Mode service.
  $entity_display_repository = \Drupal::service('entity_display.repository');

  // The entity type for which we are looking.
  $entity_type_id = "taxonomy_term";

  // Use the service to get an array of available modes.
  $view_modes = $entity_display_repository->getViewModes($entity_type_id);

  // Create a simple "allowed_values" array
  $allowed_values = array();
  foreach ($view_modes as $id => $view_mode) {
    $allowed_values[$id] = $view_mode['label'] . ' (' . $id . ')';
  }

  return $allowed_values;
}

/**
 * Create and return a list of views that are capable of rendering variations.
 *
 * Wishlist template sends the valid order id and a valid list of product ids as
 * arguments (in that order) to the selected views from within the entity.
 *
 * - The base table MUST be 'commerce_order_item'.
 * - The first argument MUST be 'order_id'.
 *   - This will be determined based on the url arguments.
 * - The second argument MUST be 'purchased_entity'
 *   - Make sure you select 'allow multiple' otherwise this won't work as expected.
 *   - Code and two queries determine all product ids that should show up.
 *   - We are using Views purely as a very customizable list generator.
 *   - We have to do it this way to support any number of taxonomy fields and
 *     any kind of term (regardless of vocabulary).
 *
 * @return array
 */
function _wishlist_template_order_item_views_values() {
  /** @var \Drupal\Core\Entity\Query\QueryFactory $query_factory */
  /** @var \Drupal\Core\Entity\Query\QueryInterface $views_query_interface */
  $query_factory = \Drupal::service('entity.query');
  $entity_manager = \Drupal::service('entity.manager');
  $views_query_interface = $query_factory->get('view');

  // Grab all the line item views.
  $views = $views_query_interface->condition('base_table','commerce_order_item')->execute();

  // "Explode" based on display versions.
  $wishlist_views = array();
  $views_default_args = array();
  foreach ($views as $view_id) {
    /** @var \Drupal\views\Entity\View $view */
    $view = $entity_manager->getStorage('view')->load($view_id);
    $view_displays = array_keys($view->get('display'));
    foreach ($view_displays as $view_display) {
      $display = $view->getDisplay($view_display);
      $argument_keys = array_keys($display['display_options']['arguments']);

      // Track default arguments. See next step for why.
      if ($view_display == "default") {
        $views_default_args[$view_id] = $argument_keys;
      }

      // Inherit default args if no argument keys are found.
      if (empty($argument_keys) && !empty($views_default_args[$view_id])) {
        $argument_keys = $views_default_args[$view_id];
      }

      // If this view has the appropriate args, return the info.
      if (in_array("order_id", $argument_keys) &&
          in_array("purchased_entity", $argument_keys)) {
        $combo_id = $view_id . '---' . $view_display;
        $label = $view->label() . ': ' . $view_display;
        $pseudo_machine_name = ' (' . $view_id .  '.' . $view_display . ')';

        // This view and display have the valid requirements to render product
        // variations.
        $wishlist_views[$combo_id] = $label . $pseudo_machine_name;
      }
    }
  }
  return $wishlist_views;
}

/**
 * Implements hook_ENTITY_TYPE_view_alter().
 *
 * @param array $build
 * @param \Drupal\wishlist_template\Entity\WishlistTemplateInterface $wishlist_template
 * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
 */
function wishlist_template_wishlist_template_view_alter(array &$build, Drupal\wishlist_template\Entity\WishlistTemplateInterface $wishlist_template, \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display) {
  $form = \Drupal::formBuilder()->getForm(\Drupal\wishlist_template\Form\CreateWishlistBasedOnTemplateForm::class, $wishlist_template);

  // This wishlist template is rendering a preview.
  $build['wishlist_template_form'] = $form;
}
